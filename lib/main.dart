import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Final Test:Firebase CRUD',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final TextEditingController _firstnameController = TextEditingController();
  final TextEditingController _lastnameController = TextEditingController();
  final TextEditingController _majorController = TextEditingController();
  final TextEditingController _student_idController = TextEditingController();


  final CollectionReference _students =
      FirebaseFirestore.instance.collection('students');


  Future<void> _createOrUpdate([DocumentSnapshot documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      _firstnameController.text = documentSnapshot['firstname'];
      _lastnameController.text = documentSnapshot['lastname'].toString();
      _majorController.text = documentSnapshot['major'].toString();
      _student_idController.text = documentSnapshot['student_id'].toString();

    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: _firstnameController,
                  decoration: const InputDecoration(labelText: 'firstname'),
                ),
                TextField(
                  controller: _lastnameController,
                  decoration: const InputDecoration(labelText: 'Lastname'),
                ),
                TextField(
                  controller: _majorController,
                  decoration: const InputDecoration(labelText: 'major'),
                ),
                TextField(
                  keyboardType:
                      const TextInputType.numberWithOptions(decimal: true),
                  controller: _student_idController,
                  decoration: const InputDecoration(
                    labelText: 'Student_id',
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String firstname = _firstnameController.text;
                    final String lastname = _lastnameController.text;
                    final String major = _majorController.text;
                    final double student_id = double.tryParse(_student_idController.text);
                    if (firstname != null && lastname != null && major != null && student_id != null) {
                      if (action == 'create') {
                        await _students
                            .add({"firstname": firstname, "lastname": lastname, "major": major, "student_id": student_id})
                            .then((value) => print("Studen Added"))
                            .catchError((error) =>
                                print("Failed to add Studen: $error"));
                      }

                      if (action == 'update') {
                        await _students
                            .doc(documentSnapshot.id)
                            .update({"name": firstname, "lastname": lastname, "major": major, "student_id": student_id})
                            .then((value) => print("Studen Updated"))
                            .catchError((error) =>
                                print("Failed to update Studen: $error"));
                      }

                      // Clear the text fields
                      _firstnameController.text = '';
                      _lastnameController.text = '';
                      _majorController.text = '';
                      _student_idController.text = '';


                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  Future<void> _deleteStuden(String StudenId) async {
    await _students
        .doc(StudenId)
        .delete()
        .then((value) => print("Studen Deleted"))
        .catchError((error) => print("Failed to delete Studen: $error"));

    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('You have successfully deleted a Studen')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Firebase Sample CRUD'),
      ),
      body: StreamBuilder(
        stream: _students.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return ListView.builder(
              itemCount: streamSnapshot.data.docs.length,
              itemBuilder: (context, index) {
                final DocumentSnapshot documentSnapshot =
                    streamSnapshot.data.docs[index];
                return Card(
                  margin: const EdgeInsets.all(10),
                  child: ListTile(
                    title: Text(documentSnapshot['firstname']),
                    subtitle: Text(documentSnapshot['lastname']+'\n'+documentSnapshot['major']+'\n' +documentSnapshot['student_id'].toString()),
                    trailing: SizedBox(
                      width: 100,
                      child: Row(
                        children: [
                          IconButton(
                              icon: const Icon(Icons.edit),
                              onPressed: () =>
                                  _createOrUpdate(documentSnapshot)),
                          IconButton(
                              icon: const Icon(Icons.delete),
                              onPressed: () =>
                                  _deleteStuden(documentSnapshot.id)

                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _createOrUpdate(),
        child: const Icon(Icons.add),
      ),
    );
  }
}
